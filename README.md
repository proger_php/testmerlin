## Тестовое задание в компанию merlin

Написать сервис, который будет сообщать насколько имя подходит человеку по его фотографии. 
Для контроля зависимостей важно использовать composer. 
Можно использовать какой либо фреймворк (symfony предпочтительно), либо использовать его части, либо любые другие библиотеки сторонних разработчиков. 
 
Сервис должен иметь одну точку входа (localhost:8000), которая может принимать два типа запроса: 
1) POST запрос, с содержимым типа multipart/form-data, с полями name и photo. 
2) GET запрос, с обычным query параметром task_id 
 
Результатом запроса должны быть: 
1) Загружена фотография во временную папку 
2) Проверено, не отправлял ли этот пользователь уже эту фотографию, в этом случае вернуть результат и status=ready. 
2) Создана задача и записана в БД/Хранилище 
3) Отдан ответ json (с соответствующим http кодом ответа): 
{"status": "received", "task": НОМЕР_ЗАДАЧИ, "result":"null или Результат_(float)"} 
 
Сервис должен сразу вернуть ответ и в фоне запустить дальнейшую работу, до полного ожидания результата: 
1) Отправить POST запрос, с содержимым типа multipart/form-data на http://merlinface.com:12345/api/ с телом, содержащим: 
[ 
  'name' => "Имя", 
  'photo' => файл фотографии, созданный посредством curl_file_create(), 
] 
2) Получить и обработать два варианта ответа 
  а)  Результат готов сразу: 
    {"status": "success", "result": "Результат_(float)"} 
  б)  Требуется время, запросите результат через пару секунд 
    {"status": "wait", "result": null, "retry_id": id_задачи_для_повторного_запроса} 
     
В случае варианта "а", нужно: 
- отметить что задача с номером НОМЕР_ЗАДАЧИ была выполнена 
- записать результат 
 
В случае варианта "б", нужно: 
- повторять запрос через пару секунд, но уже вместо фотографии и имени отправить поле retry_id со значением retry_id, полученного в ответе варианта "б".  
  До тех пор, пока не будет получен ответ: 
  {"status": "ready", "result": "Результат_(float)"} 
- отметить что задача с номером НОМЕР_ЗАДАЧИ была выполнена 
- записать результат 
 
При обращении к конечной точке сервиса(для получения результата) посредством GET запроса /?task_id=НОМЕР_ЗАДАЧИ, должен быть отдан один из трех вариантов ответов: 
1)  Результат еще не готов  (с соответствующим http кодом ответа) 
  {"status": "wait", "result": null} 
2)  Результат готов (с соответствующим http кодом ответа) 
  {"status": "ready", "result": "Результат_(float)"} 
3)  Задача с таким номером не найдена (с соответствующим http кодом ответа) 
  {"status": "not_found", "result": null} 
 
Итоговый код должен: 
1) запускаться через php -S localhost:8000 или любой веб-сервер, который входить в фреймворк, например: symfony serve 
2) содержать дамп базы, проще конечно ели это будет SQLite 
3) содержать какую то доп. инструкцию для работы скрипта, если нужно что-то сделать еще для установки и работы кода. 




<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Instructions

### Project setup
```
git clone git@gitlab.com:proger_php/testmerlin.git
```
```
cd testmerlin
```
```
composer install
```
#### rename .env.example to .env
#### Configure the section for connecting to the database in the .env file
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db
DB_USERNAME=username
DB_PASSWORD=passw0rd
```

#### Launching database migrations
In the console, execute the command
```
php artisan migrate
```
##### OR 

Load DB from dump database/merlin.sql

#### Start web server
In the console, execute the command

```
php artisan serve
```

Starting Laravel development server: http://127.0.0.1:8000


#### Start queue server
In the console, execute the command

```
php artisan queue:work
```
