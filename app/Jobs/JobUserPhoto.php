<?php

namespace App\Jobs;

use App\Models\UsersPhoto;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class JobUserPhoto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $userPhoto;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UsersPhoto $userPhoto)
    {
        $this->userPhoto = $userPhoto;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = $this->send();
        $this->userPhoto->status = $response->status;
        $this->userPhoto->result = $response->result;
        $this->userPhoto->save();

        // Delete temporary file
        Storage::delete($this->userPhoto->file_name);
    }

    /**
     * @param $retryId
     * @return mixed
     */
    private function send($retryId = null)
    {
        if ($retryId) {
            $fields = ['retry_id' => $retryId];
        } else {
            $path = Storage::path($this->userPhoto->file_name);
            $mimeType = Storage::mimeType($this->userPhoto->file_name);
            $photo = curl_file_create($path, $mimeType, basename($path));
            $fields = ['photo' => $photo, 'name' => $this->userPhoto->name];
        }

        $ch = curl_init(config('app.process_url'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: multipart/form-data"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $data = json_decode($result);

        // Delayed recursive dispatch
        if (property_exists($data, 'retry_id') && !empty($data->retry_id)) {
            sleep(config('app.process_sleep_time'));

            return $this->send($data->retry_id);
        }

        return $data;
    }
}
