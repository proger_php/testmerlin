<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestProcessGetData;
use App\Http\Requests\RequestUserPhoto;
use App\Jobs\JobUserPhoto;
use App\Models\UsersPhoto;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    /**
     * Method of obtaining information on sent photos
     *
     * @param RequestProcessGetData $request
     * @return JsonResponse
     */
    public function getData(RequestProcessGetData $request): JsonResponse
    {
        $response = ['status' => 'not_found', 'result' => null];

        if ($model = UsersPhoto::where('id', $request->get('task_id'))->first()) {
            $response = ['status' => 'wait', 'result' => null];

            if ($model->status === 'success') {
                $response = ['status' => 'ready', 'result' => $model->result];
            }

        }

        return response()->json($response);
    }

    /**
     *
     * @param RequestUserPhoto $request
     * @return JsonResponse
     */
    public function userPhotoSend(RequestUserPhoto $request): JsonResponse
    {
        $path = Storage::putFile('files', $request->file('photo'));
        $fileHash = md5(Storage::get($path));
        $name = $request->post('name');

        /*
         * We check if this user has already sent this photo,
         * in this case we return the result
         */
        if ($model = UsersPhoto::where('name', $name)->where('file_hash', $fileHash)->first()) {
            // Delete temporary file
            Storage::delete($path);

            return response()->json(
                [
                    'status' => $model->status === 'success' ? 'ready' : $model->status,
                    'task'   => $model->id,
                    'result' => $model->result,
                ]
            );
        }

        $model = new UsersPhoto();
        $model->file_name = $path;
        $model->file_hash = $fileHash;
        $model->name = $name;
        $model->status = 'received';
        $model->result = null;
        $model->save();

        // Launching Jobs in the Background
        JobUserPhoto::dispatch($model);

        return response()->json(['status' => 'received', 'task' => $model->id, 'result' => null]);
    }
}
