<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class RequestUserPhoto extends FormRequest
{
    /**
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(['status' => 'error', 'result' => null, 'task' => null, 'errors' => $validator->errors()],
                422
            )
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|mimetypes:image/jpeg,image/pjpeg,image/png,image/svg+xml,image/tiff,image/webp',
            'name' => 'required|string|max:50',
        ];
    }
}
