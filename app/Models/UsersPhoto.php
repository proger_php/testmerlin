<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * This is the model class for table "users_photo"
 *
 * @property int $id
 * @property string $name
 * @property string $file_name
 * @property string $file_hash
 * @property string $status
 * @property string $result
 */
class UsersPhoto extends Model
{
    use HasFactory;

    protected $table = 'users_photo';

    protected $fillable = ['name','file_name','file_hash','status','result'];

}
