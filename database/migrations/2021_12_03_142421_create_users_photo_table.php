<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersPhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_photo', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('file_name',255)->comment('Имя файла');
            $table->string('file_hash',255)->comment('Хеш файла');
            $table->string('status',8)->comment('Статус');
            $table->string('result',255)->comment('Результат');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_photo');
    }
}
